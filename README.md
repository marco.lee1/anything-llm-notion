# anything-llm-notion

## Installation

1. Install bun
   1. MacOS
      1. `brew install oven-sh/bun/bun`
   2. Window
      1. [Installation | Bun Docs](https://bun.sh/docs/installation#windows)

## Getting started

1. Create Notion API key
   1. Visit [Notion API](https://developers.notion.com/)
   2. Login if not already
   3. On right top corner, click View my integrations
   4. Create a new integration
   5. Type in a name and associated workspace
      1. You must be owner to this workspace
   6. Click Submit and copy the API key
   7. Back to the root repo, run `cp .env.sample .env`
   8. Paste into the right place in `.env`
2. Create AnythingLLM key
   1. Download the app if not already and sign in
   2. On the left sidebar, click on the tools logo on the bottom right
   3. Click API keys on the left sidebar
   4. Click `Generate New API key` and agree
   5. Copy the key and paste it into the right place in `.env`

## Description

Stream chat from AnythingLLM, using prompt to break down tasks and add them to Notino database
