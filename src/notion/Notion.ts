import { Inquirer, isString, type Task } from "@/utils";
import { Client, APIErrorCode, isNotionClientError } from "@notionhq/client";
import type { DatabaseObjectResponse } from "@notionhq/client/build/src/api-endpoints";

export default class Notion extends Inquirer {
  private mainTaskDbId?: string;
  private subtaskDbId?: string;
  private client: Client;

  constructor(private apiKey: string) {
    if (!isString(apiKey)) {
      throw new Error("Notion: API key cannot be empty");
    }

    super();
    this.apiKey = apiKey;

    this.client = new Client({
      auth: this.apiKey,
    });
  }

  async setDatabases(): Promise<void> {
    const databases: any[] = [];

    while (true) {
      try {
        const response = await this.client.search({
          filter: {
            property: "object",
            value: "database",
          },
        });

        if (!response.results.length) {
          throw new Error(
            "No databases found, please create one in Notion before continuing",
          );
        }

        databases.push(...(response.results as DatabaseObjectResponse[]));

        if (!response.has_more) {
          break;
        }
      } catch (error) {
        throw this.catchError(error);
      }
    }

    const mainTaskDb = await this.singleSelect<DatabaseObjectResponse>(
      databases,
      (database) => database.title[0].plain_text,
      "Select a Notion Main Task database to work with",
    );

    this.mainTaskDbId = mainTaskDb.id;

    const subtaskDb = await this.singleSelect<DatabaseObjectResponse>(
      databases,
      (database) => database.title[0].plain_text,
      "Select a Notion Subtask database to work with",
    );

    this.subtaskDbId = subtaskDb.id;
  }

  async createMainTask(task: Task): Promise<string> {
    if (!this.mainTaskDbId) {
      throw new Error("No main task database selected");
    }

    try {
      const response = await this.client.pages.create({
        parent: {
          type: "database_id",
          database_id: this.mainTaskDbId,
        },
        properties: {
          title: {
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: task.title,
                },
              },
            ],
          },
        },
      });

      console.log(`Main Task created: ${response.id}, title: ${task.title}`);
      return response.id;
    } catch (error) {
      throw this.catchError(error);
    }
  }

  async createSubtask(task: Task, mainTaskId: string): Promise<string> {
    if (!this.subtaskDbId) {
      throw new Error("No subtask database selected");
    }

    try {
      const response = await this.client.pages.create({
        parent: {
          type: "database_id",
          database_id: this.subtaskDbId,
        },
        properties: {
          title: {
            type: "title",
            title: [
              {
                type: "text",
                text: {
                  content: task.title,
                },
              },
            ],
          },
          Project: {
            type: "relation",
            relation: [
              {
                id: mainTaskId,
              },
            ],
          },
        },
        children:
          task?.description?.map((desc) => {
            return {
              object: "block",
              type: "paragraph",
              paragraph: {
                rich_text: [
                  {
                    type: "text",
                    text: {
                      content: desc,
                    },
                  },
                ],
              },
            };
          }) || [],
      });

      console.log(`Subtask created: ${response.id}, title: ${task.title}`);
      return response.id;
    } catch (error) {
      throw this.catchError(error);
    }
  }

  private catchError(error: unknown): unknown {
    if (!isNotionClientError(error)) {
      return error;
    }

    if (error.code === APIErrorCode.Unauthorized) {
      return new Error("Unauthorized, please check your Notion API key");
    }
  }
}
