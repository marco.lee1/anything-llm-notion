export const NOTION_API_KEY = process.env.NOTION_KEY;

export const ANYTHING_LLM_EP =
  process.env.ANYTHING_LLM_EP || "http://localhost:3001";

export const ANYTHING_LLM_API_KEY = process.env.ANYTHING_LLM_KEY;
