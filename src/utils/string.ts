export const isString = (str: string): str is string => {
  return !!str && str.trim() !== "";
};
