export type Task = {
  title: string;
  description?: string[];
  subTasks?: Task[];
};

export const postProcess = (response: string): Task[] => {
  const steps = response.split("\n");

  return steps.reduce((acc: Task[], step: string) => {
    if (step.includes("<#STEP#>")) {
      const title = step.replaceAll("<#STEP#>", "").trim();
      acc.push({ title });
    }

    if (step.includes("<#DESC#>")) {
      const description = step.replaceAll("<#DESC#>", "").trim();
      const last = acc.length - 1;
      acc[last].description = acc[last].description
        ? [...acc[last].description, description]
        : [description];
    }

    return acc;
  }, []);
};

export const printTasks = (tasks: Task[]): void => {
  console.log("The following tasks were identified:\n");

  for (let i = 0; i < tasks.length; i++) {
    const task = tasks[i];
    console.log(`Task ${i + 1}: ${task.title}`);
    if (task.description) {
      for (const desc of task.description) {
        console.log(`   - ${desc}`);
      }
    }
    console.log();
  }
};
